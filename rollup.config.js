import commonjs from "rollup-plugin-commonjs";
import nodeResolve from "rollup-plugin-node-resolve";

export default {
  input: "./main.js",
  output: {
    file: "./bundle.js",
    format: "iife",
    sourcemap: true,
    name: "Bundle"
  },
  plugins: [
    nodeResolve(),
    commonjs()
  ]
};
